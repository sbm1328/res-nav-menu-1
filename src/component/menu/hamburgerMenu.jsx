import { motion } from "framer-motion";
import React from "react";
import { useState } from "react";
import styled from "styled-components";
import { MenuToggle } from "./menuToggle";
import PersonIcon from "@material-ui/icons/Person";

import { MenuData } from "./menuData";

const HamburgerMenuContainer = styled.div`
  width: 100%;
  height: 55px;
  flex-direction: ${({ alignRight }) => (alignRight ? "row-reverse" : "row")};
  color: #fff;

  @media (max-width: 810px){
    display: flex;
  }

  @media (min-width: 811px){
    display: none;
  }
`;

const HamburgerIcon = styled.div`
  color: ${({ reverseColor }) => (reverseColor ? "#000" : "#fff")};
  cursor: pointer;
  z-index: 99;
  transition: all 250ms ease-in-out;
  padding: 1em;
`;

const MenuContainer = styled(motion.div)`
  min-width: 250px;
  width: 100%;
  max-width: 300px;
  height: 100%;
  background-color: #fff;
  box-shadow: -2px 0 2px rgba(15, 15, 15, 0.3);
  z-index: 90;
  position: fixed;
  top: 0;
  right: ${({ alignRight }) => alignRight && 0};
  left: ${({ alignRight }) => !alignRight && 0};
  transform: ${({ alignRight }) =>
    alignRight ? "translateX(100%)" : "translateX(-100%)"};
  user-select: none;
  /* padding: 1em 2.5em; */
  color: black;
  background-image: linear-gradient(-135deg, #1400c8, #b900b4, #f50000);
`;

const menuRightVariants = {
  open: {
    transform: "translateX(0%)",
  },
  closed: {
    transform: "translateX(100%)",
  },
};

const menuLeftVariants = {
  open: {
    transform: "translateX(0%)",
  },
  closed: {
    transform: "translateX(-100%)",
  },
};

const menuTransition = {
  type: "spring",
  duration: 1,
  stiffness: 33,
  delay: 0.1,
};

const MenuProfile = styled.div`
  width: 100%;
  /* background-color: #b2afaf; */
  height: 25vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  border-bottom: 1px solid #4f4f4f;
`;

const MenuBody = styled.div`
  width: 100%;
  height: 75vh;
`;

const CustomPersonIcon = styled(PersonIcon)`
  font-size: 7em !important;
  color: #fff;
  border: 5px solid #fff;
  border-radius: 100px;
`;

const MenuProfileName = styled.p`
  font-size: 24px;
  margin: 10px 0 0 0;
  padding: 0;
  color: #fff;
  font-weight: 700;
`;

const MenuItem = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  width: 100%;
  height: 69vh;
`;

const MenuFooter = styled.div`
  width: 100%;
  height: 6vh;
  border-top: 1px solid #4f4f4f;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const MenuFooterText = styled.p`
  font-size: 18px;
  margin: 0;
  padding: 0;
  color: #fff;
`;

const MenuLi = styled.li`
  margin: 0;
  padding: 0;
  height: 60px;

  border-bottom: 1px solid #8a307f;
  display: flex;
  align-items: center;
  cursor: pointer;
  padding: 0 3em;
  transition: border-left 100ms ease-in-out;
  border-left: 0px solid #fff;
  &:hover {
    border-left: 2px solid #fff;
  }
`;

const MenuItemText = styled.p`
  font-size: 22px;
  margin: 0;
  padding: 0;
  color: #fff;
  margin-left: 32px;
`;

export default function HamburgerMenu(props) {
  const [isOpen, setOpen] = useState(false);

  const toggleMenu = () => {
    setOpen(!isOpen);
  };

  //alignRight

  return (
    <HamburgerMenuContainer >
      <HamburgerIcon>
        <MenuToggle toggle={toggleMenu} isOpen={isOpen} />
      </HamburgerIcon>

      <MenuContainer
        initial={false}
        animate={isOpen ? "open" : "closed"}
        variants={false ? menuRightVariants : menuLeftVariants}
        transition={menuTransition}
      >
        <MenuProfile>
          <CustomPersonIcon />
          <MenuProfileName>Profile Name</MenuProfileName>
        </MenuProfile>
        <MenuBody>
          <MenuItem>
            {MenuData.map((val) => (
              <MenuLi>
                {val.icon}
                <MenuItemText>{val.menuTitle}</MenuItemText>
              </MenuLi>
            ))}
          </MenuItem>
          <MenuFooter>
            <MenuFooterText>Version : V1.0.0</MenuFooterText>{" "}
          </MenuFooter>
        </MenuBody>
      </MenuContainer>
    </HamburgerMenuContainer>
  );
}
