import React from "react";
import styled from "styled-components";
import { MenuData } from "./menuData";
import { motion } from "framer-motion";

const MenuContainer = styled.div`
  width: 100vw;
  height: 55px;
  display: flex;
  flex-direction: row;
  background-image: linear-gradient(-10deg, #1400c8, #b900b4, #f50000);
  align-items: center;
  justify-content: center;

  @media (max-width: 810px){
    display: none;
  }

  @media (min-width: 811px){
    display: flex;
  }
`;

const MenuDiv = styled.div`
  max-width: 1200px;
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const MenuLeft = styled.div`
  width: 10%;
  height: 100%;
  display: flex;
  flex: row;
  align-items: center;
  justify-content: center;
`;
const MenuRight = styled.div`
  width: 90%;
  height: 100%;
  display: flex;
  flex: row;
  align-items: center;
  justify-content: flex-end;
  /* margin: 0 20px; */
`;

const NavLogo = styled.img`
  width: 45px;
  height: 45px;
`;

const NavItem = styled(motion.p)`
  font-size: 22px;
  margin: 0;
  padding: 0;
  color: #fff;
  margin-left: 16px;
  padding: 0 20px;
  cursor: pointer;
  font-weight: 550;
`;
// DesktopMenu Component
export default function DesktopMenu() {
  return (
    <MenuContainer>
      <MenuDiv>
        <MenuLeft>
          <NavLogo src={require("../../assets/images/logo192.png").default} />
        </MenuLeft>
        <MenuRight>
          {MenuData.map((val) => (
            <NavItem
              whileHover={{
                textDecoration: "underline",
                scale: 1.1,
              }}
              onDurationChange={1}
            >
              {val.menuTitle}
            </NavItem>
          ))}
        </MenuRight>
      </MenuDiv>
    </MenuContainer>
  );
}
