import * as Feather from "react-icons/fi";
import * as FontAwesome from "react-icons/fa";

export const MenuData = [
  { menuTitle: "Home", icon: <FontAwesome.FaHome size={24} color="#fff" /> },
  { menuTitle: "Airbnb", icon: <FontAwesome.FaAirbnb size={24} color="#fff" /> },
  // { menuTitle: "Adobe", icon: <FontAwesome.FaAdobe size={24} color="#fff" /> },
  // { menuTitle: "Android", icon: <FontAwesome.FaAndroid size={24} color="#fff" /> },
  { menuTitle: "Apple", icon: <FontAwesome.FaApple size={24} color="#fff" /> },
  { menuTitle: "Bitcoin", icon: <FontAwesome.FaBitcoin size={24} color="#fff" /> },
  { menuTitle: "Facebook", icon: <FontAwesome.FaFacebookF size={24} color="#fff" /> },
  { menuTitle: "Linkedin", icon: <FontAwesome.FaLinkedinIn size={24} color="#fff" /> },
];
