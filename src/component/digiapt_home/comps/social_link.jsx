import React, { useState, useEffect } from "react";
import styled from "styled-components";
import * as FontAwesome from "react-icons/fa";

const defaultData = [
  {
    icon: <FontAwesome.FaFacebookF />,
  },
  {
    icon: <FontAwesome.FaLinkedinIn />,
  },
];

const LinkContainer = styled.div`
  display: flex;
  flex-direction: row;
  border-radius: 5px;
  border: 1px solid #e4e4e4;

  &::first-child {
    border-radius: 5px 0px 0px 5px;
  }
  &::last-child {
    border-radius: 0px 5px 5px 0px;
  }
  background-color: white;
`;
const LinkImg = styled.div`
  width: 30px;
  height: 30px;
  border-right: ${({ last }) => (last ? "" : "1px solid #e4e4e4")};
  font-size: 15px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  color: black;
`;

// SocialLink Component
export default function SocialLink(props) {
  const [social, setSocial] = useState(defaultData);
  useEffect(() => {
    if (props.data) {
      setSocial(props.data);
    }
  }, []);

  return (
    <LinkContainer>
      {social?.map((val, index) => (
        <LinkImg last={val.length - 1 === index}>{val.icon}</LinkImg>
      ))}
    </LinkContainer>
  );
}
