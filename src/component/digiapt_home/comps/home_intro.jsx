import React from "react";
import styled from "styled-components";
import { Button } from "@material-ui/core";

const HomeContainer = styled.div`
  max-width: 1200px;
  display: flex;
  flex-direction: row;
  width: 100%;
`;
const HomeLeft = styled.div`
  width: 50%;
  text-align: left;
`;
const HomeRight = styled.div`
  width: 50%;
`;
const HomeIntroText = styled.p`
  font-family: Poppins, sans-serif;
  color: #fff;
  font-size: 48px;
  line-height: 58px;
  font-weight: 500;
  text-transform: none;
  margin: 0 0 20px 0;
`;
const HomeIntroDes = styled.p`
  margin-top: 10px;
  color: hsla(0, 0%, 100%, 0.9);
  font-size: 16px;
  line-height: 26px;
  font-weight: 400;
  text-align: left;
  width: 70%;
  margin-bottom: 10px;
  font-family: Nunito, sans-serif;
`;
const HomeIntoBtn = styled(Button)`
  && {
    position: relative;
    display: block;
    float: none;
    clear: none;
    border-width: 0px;
    border-color: transparent;
    border-radius: 5px;
    background-color: #ff790f;
    -webkit-transition: all 400ms ease;
    transition: all 400ms ease;
    text-align: center;
    overflow: hidden;
    width: 185px;
    padding: 12px 25px;
    align-items: center;
    border: 1px none #0675ec;
    font-family: Poppins, sans-serif;
    color: #fff;
    text-decoration: none;
    margin-top: 30px;
  }
`;
const HomeIntoImg = styled.img``;

// HomeIntro Component
export default function HomeIntro() {
  return (
    <HomeContainer>
      <HomeLeft>
        <HomeIntroText>
          Digital is the future.
          {<br />}
          Are you ready?
          {<br />}
          Let us help
        </HomeIntroText>
        <HomeIntroDes>
          Make a dash to the finish line. Leap with us as your technology
          partner.
        </HomeIntroDes>
        <HomeIntoBtn>DISCOVER HOW</HomeIntoBtn>
      </HomeLeft>
      <HomeRight></HomeRight>
    </HomeContainer>
  );
}
