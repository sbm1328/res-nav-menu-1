import React from "react";
import styled from "styled-components";
import SocialLink from "./social_link";

const NavMain = styled.div`
  height: 75px;
  position: fixed;
  z-index: 1000;
  width: 100%;
  top: 0;
  z-index: 1000;
`;
const NavContainer = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 5%;
`;
const NavleftDiv = styled.div`
  width: 20%;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;
const NavRightDiv = styled.div`
  width: 80%;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`;
const NavLogo = styled.img`
  width: 40px;
  height: 40px;
`;
const NavLogoText = styled.p`
  font-weight: 700;
  letter-spacing: 1px;
  text-transform: none;
  color: black;
  font-family: Poppins, sans-serif;
  font-size: 28px;
  line-height: 30px;
`;
const NavMenuText = styled.p`
  padding: 10px 20px;
  cursor: pointer;
  font-family: Nunito, sans-serif;
  font-size: 15px;
  line-height: 25px;
  font-weight: 600;
  color: white;
`;

const menuData = [
  {
    text: "Home",
    goTo: "",
  },
  {
    text: "Service",
    goTo: "",
  },
  {
    text: "Portfolio",
    goTo: "",
  },
  {
    text: "About Us",
    goTo: "",
  },
  {
    text: "Contact",
    goTo: "",
  },
];

// Nav Component
export default function Nav() {
  return (
    <NavMain>
      <NavContainer>
        <NavleftDiv>
          <NavLogo src="https://uploads-ssl.webflow.com/5ef1048d6e07c57645d70ebb/5f1a3d366c10b823b1ab9478_logo-footer%20(1).png" />
          <NavLogoText>Digiapt</NavLogoText>
        </NavleftDiv>
        <NavRightDiv>
          {menuData.map((val, i) => (
            <NavMenuText>{val.text} </NavMenuText>
          ))}
          <SocialLink />
        </NavRightDiv>
      </NavContainer>
    </NavMain>
  );
}
