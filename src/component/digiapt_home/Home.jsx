import React from "react";
import Nav from "./comps/Nav";
import styled from "styled-components";
import HomeIntro from "./comps/home_intro";

const HeroBanner = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-image: url(https://uploads-ssl.webflow.com/5ef1048d6e07c57645d70ebb/5ef1048db122b651f12b8aef_Subscribeww.png);
  background-position: 0px 0px, 0px 0px;
  background-size: cover, auto;
`;

// Home Component
export default function Home() {
  return (
    <>
      <Nav />
      {/* <HeroBanner>
        <HomeIntro />
      </HeroBanner> */}
    </>
  );
}
