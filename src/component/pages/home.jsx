import React from "react";
import styled from "styled-components";

const Container = styled.div`
  width: 100vw;
  height: 90vh;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 32px;
  font-family: "Poppins";
`;

// Home Component
export default function Home() {
  return <Container>Home</Container>;
}
