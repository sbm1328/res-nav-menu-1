import "./App.css";
import HamburgerMenu from "./component/menu/hamburgerMenu";
import DigiaptHome from "./component/digiapt_home/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./component/pages/home";
import { useEffect } from "react";
import DesktopMenu from "./component/menu/desktopMenu";

function App() {
  return (
    <Router>
      <HamburgerMenu />
      <DesktopMenu />
      <Switch>
        <Route
          path="/"
          render={({ match: { path } }) => (
            <>
              <Route path={`${path}`} component={Home} exact />
            </>
          )}
        />
        <Route
          path="/digiapt"
          render={({ match: { path } }) => (
            <>
              <Route path={`${path}`} component={DigiaptHome} exact />
            </>
          )}
        />
      </Switch>
    </Router>
  );
}

export default App;
