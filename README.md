# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Depedency used
* [Styled Components](https://styled-components.com/)
* [React Router DOM](https://www.npmjs.com/package/react-router-dom)
* [React Icons](https://react-icons.github.io/react-icons/)
* [Framer Motion](https://www.framer.com/)


## Demo
![Demo](https://bitbucket.org/sbm1328/res-nav-menu-1/raw/7cec38497031ac3a21b4846471b70c6ff783348e/src/assets/nav-menu.gif)